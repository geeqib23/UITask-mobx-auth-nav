/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  StatusBar,
  View,
  Text,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import LandingScreen from './screens/LandingScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from './screens/LoginScreen';
import SignupScreen from './screens/SignupScreen';
import HomeScreen from './screens/HomeScreen';
import AppIntroSlider from 'react-native-app-intro-slider';
// import auth from '@react-native-firebase/auth';
// import {GoogleSignin} from '@react-native-community/google-signin';

// GoogleSignin.configure({
//   webClientId:
//     '956814842732-1ucu87i3k8bnabltsjno2dhs748rnbum.apps.googleusercontent.com',
// });

const App = () => {
  // async function onGoogleButtonPress() {
  //   console.log('Function invoked');
  //   const {idToken} = await GoogleSignin.signIn();
  //   const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  //   return auth().signInWithCredential(googleCredential);
  // }

  const Stack = createNativeStackNavigator();
  const [showRealApp, setShowRealApp] = useState(false);
  const onDone = () => {
    setShowRealApp(true);
  };
  const onSkip = () => {
    setShowRealApp(true);
  };

  const SigninButton = () => {
    return (
      // <TouchableHighlight
      // onPress={() => props.navigation.navigate('Login')}
      // >
      <View style={styles.button}>
        <Text style={styles.loginText}>Skip</Text>
      </View>
      // </TouchableHighlight>
    );
  };

  const SignupButton = () => {
    return (
      // <TouchableHighlight
      // onPress={() => props.navigation.navigate('Signup')}
      // >
      <View style={[styles.button, styles.signupButton]}>
        <Text style={styles.signupText}>Next</Text>
      </View>
      // </TouchableHighlight>
    );
  };
  const slides = [
    {
      key: 1,
      title: 'Follow',
      text: 'You can follow favourite people anytime                                                                                 and get their updates right on your                                    News Feed',
      image: {
        uri: 'https://cdn3.iconfinder.com/data/icons/menu-icons-2/7/11-512.png',
      },
      backgroundColor: '#38bf90',
    },
    {
      key: 2,
      title: 'Follow',
      text: 'You can follow favourite people anytime                                                                                 and get their updates right on your                                    News Feed',
      image: {
        uri: 'https://cdn.pngsumo.com/iphone-white-phone-cartoon-mobile-phone-phone-png-transparent-phonepng-418_851.jpg',
      },
      backgroundColor: '#fff',
    },
  ];

  const RenderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 100,
        }}>
        <Text style={styles.introTitleStyle}>{item.title}</Text>
        <Image
          style={[styles.introImageStyle, {height: item.key == 2 ? 410 : 200}]}
          source={item.image}
        />
        <Text
          style={[
            styles.introTextStyle,
            {color: item.key === 2 ? '#71bea2' : 'white'},
          ]}>
          {item.text}
        </Text>
      </View>
    );
  };

  return (
    <>
      <StatusBar backgroundColor="#2abf88" />
      {showRealApp ? (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name="Landing"
              component={LandingScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Login"
              options={{
                headerShown: false,
              }}>
              {props => (
                <LoginScreen
                  {...props}
                  // onGoogleButtonPress={onGoogleButtonPress}
                />
              )}
            </Stack.Screen>
            <Stack.Screen
              name="Signup"
              component={SignupScreen}
              options={{
                title: 'Sign Up',
                headerStyle: {
                  backgroundColor: '#2abf88',
                },
                headerTitleAlign: 'center',
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                },
              }}
            />
            <Stack.Screen
              name="Home"
              component={HomeScreen}
              options={{
                title: 'Feed',
                headerStyle: {
                  backgroundColor: '#2abf88',
                },
                headerTitleAlign: 'center',
                headerTintColor: '#fff',
                headerTitleStyle: {
                  fontWeight: 'bold',
                },
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          onDone={onDone}
          showSkipButton={true}
          onSkip={onSkip}
          renderDoneButton={SigninButton}
          renderNextButton={SignupButton}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  titleStyle: {
    padding: 10,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  paragraphStyle: {
    padding: 20,
    textAlign: 'center',
    fontSize: 16,
  },
  introImageStyle: {
    width: 200,
    height: 410,
  },
  introTextStyle: {
    fontSize: 15,
    color: 'white',
    textAlign: 'center',
    paddingVertical: 30,
  },
  introTitleStyle: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
    fontWeight: 'bold',
  },
  button: {
    // margin: 10,
    paddingHorizontal: 36,
    paddingVertical: 13,
    borderRadius: 30,
    backgroundColor: 'white',
  },
  loginText: {
    color: '#38bf90',
    fontWeight: 'bold',
    fontSize: 17,
  },
  signupText: {
    fontWeight: 'bold',
    fontSize: 17,
    color: 'white',
  },
  signupButton: {
    backgroundColor: '#38bf90',
    borderColor: 'white',
    borderWidth: 1,
  },
});

export default App;
