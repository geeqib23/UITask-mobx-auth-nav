import React from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
// import LoginScreen from './LoginScreen';

function HomeScreen({navigation}) {
  // const Drawer = createDrawerNavigator();
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => <Icon name="menu" size={30} color="white" />,
      headerRight: () => <Icon name="bell-outline" size={30} color="white" />,
    });
  }, [navigation]);
  return (
    // <NavigationContainer>
    //   <Drawer.Navigator initialRouteName="Home">
    //     <Drawer.Screen name="Home" component={HomeScreen} />
    //     <Drawer.Screen name="Notifications" component={LoginScreen} />
    //   </Drawer.Navigator>
    // </NavigationContainer>
    <View>
      <Text>Home</Text>
    </View>
  );
}

// const DrawerComponent = () => {
//   const DrawerItems = [
//     {
//       name: 'Profile',
//       iconType: 'Material',
//       iconName: 'face-profile',
//     },
//     {
//       name: 'Settings',
//       iconType: 'Feather',
//       iconName: 'settings',
//     },
//     {
//       name: 'Saved Items',
//       iconType: 'Material',
//       iconName: 'bookmark-check-outline',
//     },
//     {
//       name: 'Refer a Friend!',
//       iconType: 'FontAwesome5',
//       iconName: 'user-friends',
//     },
//   ];

//   return (
//     // <NavigationContainer>
//     //   <Drawer.Navigator
//     //     // drawerType="front"
//     //     initialRouteName="Home"
//     //     // drawerContentOptions={{
//     //     //   activeTintColor: '#e91e63',
//     //     //   itemStyle: {marginVertical: 10},
//     //     // }}
//     //   >
//     //     {DrawerItems.map(drawer => (
//     //       <Drawer.Screen
//     //         key={drawer.name}
//     //         name={drawer.name}
//     //         options={{
//     //           component: {HomeScreen},
//     //         }}
//     //       />
//     //     ))}
//     //   </Drawer.Navigator>
//     // </NavigationContainer>
//   );
// };

export default HomeScreen;

//     color={focused ? "#e91e63" : "black"}
// drawerIcon:({focused})=>
//  drawer.iconType==='Material' ?
//   <MaterialCommunityIcons
//       name={drawer.iconName}
//       size={24}
//       color={focused ? "#e91e63" : "black"}
//   />
// :
// drawer.iconType==='Feather' ?
//   <Feather
//     name={drawer.iconName}
//     size={24}
//     color={focused ? "#e91e63" : "black"}
//   />
// :
//   <FontAwesome5
//     name={drawer.iconName}
//     size={24}
