/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  Image,
  TouchableHighlight,
} from 'react-native';
import {AppStyles} from '../AppStyles';

function LandingScreen(props) {
  return (
    <View style={styles.container}>
      <Image
        source={{
          uri: 'https://cdn3.iconfinder.com/data/icons/menu-icons-2/7/11-512.png',
        }}
        style={styles.image}
      />
      <View style={styles.textContainer}>
        <Text style={styles.followText}>Follow</Text>
        <Text style={{fontSize: 15, color: 'white'}}>
          You can follow favourite people anytime
        </Text>
        <Text style={{fontSize: 15, color: 'white'}}>
          and get their updates right on your
        </Text>
        <Text style={{fontSize: 15, color: 'white'}}>News Feed</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <TouchableHighlight onPress={() => props.navigation.navigate('Login')}>
          <View style={styles.button}>
            <Text style={styles.loginText}>SIGN IN</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => props.navigation.navigate('Signup')}>
          <View style={[styles.button, styles.signupButton]}>
            <Text style={styles.signupText}>SIGN UP</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#38bf90',
  },
  image: {
    width: 200,
    height: 200,
    marginBottom: 100,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    // backgroundColor: 'red',
    width: '90%',
    position: 'absolute',
    bottom: 30,
    // marginTop: 30,
  },
  button: {
    margin: 10,
    paddingHorizontal: 36,
    paddingVertical: 13,
    borderRadius: 30,
    backgroundColor: 'white',
  },
  loginText: {
    color: '#38bf90',
    fontWeight: 'bold',
    fontSize: 17,
  },
  signupText: {
    fontWeight: 'bold',
    fontSize: 17,
    color: 'white',
  },
  signupButton: {
    backgroundColor: 'transparent',
    borderColor: 'white',
    borderWidth: 1,
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  followText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default LandingScreen;
