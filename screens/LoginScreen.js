/* eslint-disable curly */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Users} from '../store/UserStore';
// import auth from '@react-native-firebase/auth';

function LoginScreen({navigation, onGoogleButtonPress}) {
  // const [authenticated, setAutheticated] = useState(false);

  // auth().onAuthStateChanged(user => {
  //   if (user) {
  //     setAutheticated(true);
  //   }
  // });

  // React.useEffect(() => {
  //   if (authenticated) navigation.push('Home');
  // }, [authenticated, navigation]);

  useEffect(() => {
    Users.updateUsers();
  }, []);

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  return (
    <View style={styles.container}>
      <Text style={styles.brandLogo}>ramro</Text>
      <TouchableHighlight>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Login with Facebook</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={onGoogleButtonPress}>
        <View style={[styles.button, {backgroundColor: '#dd4c3a'}]}>
          <Text style={styles.buttonText}>Login with Google{'    '}</Text>
        </View>
      </TouchableHighlight>
      <Text style={styles.or}>OR</Text>
      <View style={styles.searchSection}>
        <Icon
          style={styles.searchIcon}
          name="user-circle-o"
          size={27}
          color="grey"
        />
        <TextInput
          style={styles.input}
          placeholder="Username"
          onChangeText={setName}
          underlineColorAndroid="transparent"
        />
      </View>
      <View style={styles.searchSection}>
        <Icon
          style={styles.searchIcon}
          name="unlock-alt"
          size={27}
          color="grey"
        />
        <TextInput
          style={styles.input}
          placeholder="  Password"
          onChangeText={setPassword}
          underlineColorAndroid="transparent"
        />
      </View>
      <TouchableHighlight
        onPress={() => {
          // console.log(Users.checkUserExists({name, password}));
          if (name && password) {
            if (Users.checkUserExists({name, password}))
              navigation.navigate('Home');
            else alert('Wrong username/password');
          } else alert('Field cannot be empty');
        }}>
        <View style={[styles.button, styles.signupButton]}>
          <Text style={styles.signupText}>LOGIN</Text>
        </View>
      </TouchableHighlight>
      <View style={styles.bottomFlex}>
        <Text
          onPress={() => navigation.push('Signup')}
          style={{color: '#2abd88'}}>
          Create an account
        </Text>
        <Text style={{color: '#2abd88'}}>Forgot Password</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: 'white',
  },
  brandLogo: {
    marginTop: 110,
    fontFamily: 'Bluestar-BoldItalic',
    fontSize: 80,
    color: '#d64c3e',
  },
  button: {
    width: '90%',
    backgroundColor: '#4966a0',
    margin: 10,
    paddingHorizontal: 100,
    paddingVertical: 17,
    borderRadius: 30,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
  },
  or: {
    color: 'gray',
    opacity: 0.7,
    fontWeight: '700',
    margin: 30,
  },
  searchSection: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 26,
    marginBottom: 18,
    // backgroundColor: '#fff',
  },
  searchIcon: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff',
    color: '#424242',
  },
  signupButton: {
    backgroundColor: '#2abd88',
    borderColor: 'white',
    borderWidth: 1,
    margin: 10,
    paddingHorizontal: 135,
    paddingVertical: 18,
    borderRadius: 30,
  },
  signupText: {
    fontWeight: '600',
    fontSize: 17,
    color: 'white',
  },
  bottomFlex: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '83%',
    // borderWidth: 2,
    marginTop: 10,
  },
});

export default LoginScreen;
