import {observer} from 'mobx-react-lite';
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {Users} from '../store/UserStore';

const SignupScreen = observer(({navigation}) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <View style={styles.cameraIcon}>
        <Icon name="camera-outline" size={60} color="white" />
      </View>

      <TextInput
        style={styles.input}
        placeholder="Name"
        onChangeText={setName}
        value={name}
      />
      <TextInput
        style={styles.input}
        placeholder="Email address"
        onChangeText={setEmail}
        value={email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        onChangeText={setPassword}
        value={password}
      />
      <TouchableHighlight
        onPress={() => {
          if (name && email && password) {
            Users.addUser({name, email, password});
            navigation.push('Login');
          } else alert('Field cannot be empty');
        }}>
        <View style={[styles.button, styles.signupButton]}>
          <Text style={styles.signupText}>SIGN UP</Text>
        </View>
      </TouchableHighlight>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
  },
  cameraIcon: {
    margin: 80,
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2abf88',
    // borderWidth: 1,
    borderRadius: 1000000,
  },
  input: {
    height: 50,
    margin: 12,
    padding: 10,
    width: '85%',
    borderWidth: 2,
    borderColor: 'lightgray',
    // opacity: 0.2,
    color: 'black',
    borderRadius: 5,
  },
  signupText: {
    fontWeight: 'bold',
    fontSize: 17,
    color: 'white',
  },
  signupButton: {
    backgroundColor: '#38bf90',
    borderColor: 'white',
    borderWidth: 1,
    margin: 10,
    paddingHorizontal: 135,
    paddingVertical: 18,
    borderRadius: 30,
  },
});

export default SignupScreen;
