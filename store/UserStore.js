/* eslint-disable curly */
import {observable, action, makeObservable} from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class UserStore {
  users = observable([]);
  constructor() {
    makeObservable(this, {
      users: observable,
      addUser: action,
      //   checkUserExists: computed,
    });
  }

  async addUser({name, email, password}) {
    // console.log(this.users);
    if (name && email && password) {
      this.users.push({name, email, password});
      try {
        await AsyncStorage.setItem('users', JSON.stringify(this.users));
      } catch (e) {
        console.log('Error saving');
      }
    }
    console.log(this.users);
  }

  async updateUsers() {
    try {
      const jsonValue = await AsyncStorage.getItem('users');
      this.users = jsonValue != null ? JSON.parse(jsonValue) : [];
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      console.log('error reading');
    }
  }

  checkUserExists({name, password}) {
    // console.log(this.updateUsers());
    // console.log(this.users);
    const user = this.users.filter(
      u => u.name === name && u.password === password,
    );
    // console.log(user.length);
    if (user.length) return true;
    else return false;
  }
}

export const Users = observable(new UserStore());
